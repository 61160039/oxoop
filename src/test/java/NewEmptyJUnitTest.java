/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class NewEmptyJUnitTest {

    public NewEmptyJUnitTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

    @Test
    public void testCheckWinRow1() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table data = new Table(o, x);
        data.setRowCol(1, 1);
        data.setRowCol(1, 2);
        data.setRowCol(1, 3);
        assertEquals(true, data.checkWin());

    }

    public void testCheckWinRow2() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table data = new Table(o, x);
        data.setRowCol(2, 1);
        data.setRowCol(2, 2);
        data.setRowCol(2, 3);
        assertEquals(true, data.checkWin());

    }

    public void testCheckWinRow3() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table data = new Table(o, x);
        data.setRowCol(3, 1);
        data.setRowCol(3, 2);
        data.setRowCol(3, 3);
        assertEquals(true, data.checkWin());

    }
    
    public void testCheckWinCol1() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table data = new Table(o, x);
        data.setRowCol(1, 1);
        data.setRowCol(2, 1);
        data.setRowCol(3, 1);
        assertEquals(true, data.checkWin());

    }
    
    public void testCheckWinCol2() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table data = new Table(o, x);
        data.setRowCol(1, 2);
        data.setRowCol(2, 2);
        data.setRowCol(3, 2);
        assertEquals(true, data.checkWin());

    }
        
    public void testCheckWinCol3() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table data = new Table(o, x);
        data.setRowCol(1, 3);
        data.setRowCol(2, 3);
        data.setRowCol(3, 3);
        assertEquals(true, data.checkWin());

    }
    
    public void testCheckWinX1() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table data = new Table(o, x);
        data.setRowCol(1, 1);
        data.setRowCol(2, 2);
        data.setRowCol(3, 3);
        assertEquals(true, data.checkWin());

    }
    
    public void testCheckWinX2() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table data = new Table(o, x);
        data.setRowCol(3, 1);
        data.setRowCol(2, 2);
        data.setRowCol(1, 3);
        assertEquals(true, data.checkWin());

    }

    public void testSwitchTurn() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table data = new Table(o, x);
        if (data.getCurrentPlayer().getName() == 'X') {
            data.switchTurn();
            assertEquals('O', data.getCurrentPlayer().getName());
        } else {
            data.switchTurn();
            assertEquals('X', data.getCurrentPlayer().getName());
        }
    }
    
    public void testSetPointWin() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table data = new Table(o, x);
        data.setRowCol(3, 1);
        data.setRowCol(2, 2);
        data.setRowCol(1, 3);
        data.checkWin();
        assertEquals(1,data.getWinner().getWin());

    }
    
    public void testSetPointLose() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table data = new Table(o, x);
        data.setRowCol(3, 1);
        data.setRowCol(2, 2);
        data.setRowCol(1, 3);
        data.checkWin();
        assertEquals(0,data.getWinner().getLose());

    }
    

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
//     public void hello() {}
}
