
import java.util.*;

/**
 *
 * @author james
 */
class Table {

    Random rd = new Random();
    private char data[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player o;
    private Player x;
    private Player currentPlayer;
    private Player win;
    private int turn;

    public Table(Player o, Player x) {
        this.o = o;
        this.x = x;
        if (rd.nextInt(2) + 1 == 1) {
            currentPlayer = o;
        } else {
            currentPlayer = x;
        }
        win = null;
    }

    public char[][] getData() {
        return data;
    }

    public Player getWinner() {
        return win;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public int getTurn() {
        return turn;
    }

    public void setWin() {
        if (win == o) {
            o.win();
            x.lose();
        } else if (win == x) {
            x.win();
            o.lose();
        }
    }

    public boolean checkDraw() {
        if (turn == 9) {
            win = null;
            x.draw();
            o.draw();
            return true;
        }
        return false;
    }

    public boolean checkWin() {
        for (int i = 0; i < 3; i++) {
            if (checkRow(i)) {
                return true;
            } else if (checkCol(i)) {
                return true;
            } else if (checkX1()) {
                return true;
            } else if (checkX2()) {
                return true;
            }
        }
        switchTurn();
        return false;
    }

    public boolean checkRow(int row) {
        for (int r = 0; r < 3; r++) {
            if (data[row][r] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWin();
        return true;
    }

    public boolean checkCol(int col) {
        for (int c = 0; c < 3; c++) {
            if (data[c][col] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWin();
        return true;
    }

    public boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (data[i][i] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWin();
        return true;
    }

    public boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (data[2 - i][0 + i] != currentPlayer.getName()) {
                return false;
            }
        }
        win = currentPlayer;
        setWin();
        return true;
    }

    public void switchTurn() {
        currentPlayer = currentPlayer == o ? x : o;
    }

    public boolean setRowCol(int row, int col) {
        if (data[row - 1][col - 1] == ('-')) {
            data[row - 1][col - 1] = currentPlayer.getName();
            turn++;
            return true;
        }
        return false;
    }

}
